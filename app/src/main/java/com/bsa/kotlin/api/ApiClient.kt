package com.bsa.kotlin.api

import com.bsa.kotlin.data.CommentsSearchResponse
import com.bsa.kotlin.data.PostSearchResponse
import com.bsa.kotlin.data.UserSearchResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiClient {
    @GET("posts")
    suspend fun getPosts(): List<PostSearchResponse>

    @GET("posts/{postId}/comments")
    suspend fun getComments(
            @Path("postId") postId: String
    ): List<CommentsSearchResponse>

    @GET("users/{userId}")
    suspend fun getUser(
            @Path("userId") userId: String
    ): UserSearchResponse

    companion object {
        private const val BASE_URL = "http://jsonplaceholder.typicode.com/"

        fun create(): ApiClient {
            val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()

            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ApiClient::class.java)
        }
    }

}
