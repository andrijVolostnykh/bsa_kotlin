package com.bsa.kotlin

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bsa.kotlin.adapters.PostAdapter
import com.bsa.kotlin.viewmodels.PostListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class PostsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val model: PostListViewModel by viewModels()
        val adapter = PostAdapter(emptyList())
        recycler_view.adapter = adapter

        model.fetchPosts()
        model.posts.observe(this, Observer { posts ->
            adapter.refreshPosts(posts)
        })

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
    }

}
