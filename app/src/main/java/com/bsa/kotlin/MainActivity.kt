package com.bsa.kotlin

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bsa.kotlin.adapters.PostAdapter
import com.bsa.kotlin.viewmodels.PostListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val commentsHeader: TextView = findViewById(R.id.comments_header)
        val commentRecyclerView: RecyclerView = findViewById(R.id.comments_recycler_view)

        val model: PostListViewModel by viewModels()
        val adapter = PostAdapter(emptyList())
        recycler_view.adapter = adapter

        model.fetchPosts()
        model.posts.observe(this, Observer { posts ->
            adapter.refreshPosts(posts)
        })

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)


        fun showComments(view: View) {
            if(commentsHeader.visibility == View.GONE) {
                commentsHeader.visibility = View.VISIBLE
                commentRecyclerView.visibility = View.VISIBLE
            } else {
                commentsHeader.visibility = View.GONE
                commentRecyclerView.visibility = View.GONE
            }

        }
    }

}
